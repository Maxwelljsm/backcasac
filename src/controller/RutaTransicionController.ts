// En tu controlador o donde manejes las rutas
import { Request, Response } from "express";
import { getRepository } from "fireorm";
import RespuestaTransicions from "../entity/RutaTransicions";
class RutaTransicionController {
  async  CrearRutaTransicion(req: Request, res: Response) {
    try {
      const {
        nombre,
        fechaCreacion,
        sesiones,
      } = req.body;

      if (!nombre) {
        // If id is provided, return an error as it should be handled by a separate update service
        return res.status(400).json({ error: "El campo 'id' no debe estar presente al crear una guía." });
      }
      if (!sesiones || !Array.isArray(sesiones) || sesiones.length === 0) {
        return res.status(400).json({ error: 'La propiedad "sesiones" debe ser un array no vacío.' });
      }

      const guiaRepository = getRepository(RespuestaTransicions);
      const nuevaRutaTransicion = new RespuestaTransicions();
      nuevaRutaTransicion.nombre = nombre;
      nuevaRutaTransicion.fechaCreacion = fechaCreacion;
      nuevaRutaTransicion.sesiones = sesiones;

      

      await guiaRepository.create(nuevaRutaTransicion );

      return res.status(201).json({ mensaje: "Ruta Trtansicion creada exitosamente.", RutaTransicion: nuevaRutaTransicion .nombre });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: "Error interno del servidor." });
    }
  }
  async modificarRutaTransicion(req: Request, res: Response) {
    try {
      // const { finca } = req.params;
      const { nombre,
        fechaCreacion,
        sesiones, } = req.body;
      // Validar que se proporcionen los datos necesarios
      const requiredFields = ['nombre', 'fechaCreacion', 'sesiones'];

      for (const field of requiredFields) {
        if (!req.body[field]) {
          return res.status(400).json({ error: `El campo ${field} es obligatorio para modificar el formulario.` });
        }
      }

      // Validar que sesiones sea un array no vacío
      if (!sesiones || !Array.isArray(sesiones) || sesiones.length === 0) {
        return res.status(400).json({ error: 'La propiedad "sesiones" debe ser un array no vacío.' });
      }
      // Obtener el repositorio de FireORM para la entidad Guias
      const guiasRepository = getRepository(RespuestaTransicions);

      // Obtener la guía existente desde el repositorio
      const RutaTransicionExistente = await guiasRepository.whereEqualTo('nombre', nombre).findOne();

        if (!RutaTransicionExistente) {
          return res.status(404).json({ error: 'RutaTransicion no encontrado.' });
        }
  
        // Actualizar los campos de la guía
        RutaTransicionExistente.nombre = nombre;
        RutaTransicionExistente.fechaCreacion = fechaCreacion;
        RutaTransicionExistente.sesiones = sesiones;
  
        // Guardar los cambios en la base de datos
        await guiasRepository.update(RutaTransicionExistente);
  
        return res.status(200).json({ mensaje: 'RutaTransicion modificado exitosamente.' });

    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: 'Error interno del servidor.' });
    }
  }
  
  async consultarRutaTransicion(req: Request, res: Response) {
    try {
      const { nombre } = req.params; // Suponiendo que el id de la guía se pasa como parámetro en la URL
      

      // Consultar la guía desde la base de datos
      const RutaTransicion = await getRepository(RespuestaTransicions).whereEqualTo('nombre', nombre).findOne();

      // Verificar si la guía existe
      if (!RutaTransicion) {
        return res.status(404).json({ error: "Ruta transicion no encontrado." });
      }
      // if (RutaTransicion.nombre !== nombre) {
      //   return res.status(404).json({ error: "Esta finca no tiene datos registrados de transición." });
      // }
        return res.status(200).json({ RutaTransicion });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: "Error interno del servidor." });
    }
  }

}
export default new RutaTransicionController();
