import { Request, Response } from 'express';
import * as admin from 'firebase-admin';
import * as multer from 'multer';

class UploadController {
  public upload = multer({
    storage: multer.memoryStorage()
  });

  public uploadMiddleware = this.upload.array('image');

  // Método para manejar la subida de archivos
  public subirImagen(req: Request & { files: multer.File[] }, res: Response): void {
    // req.file estará disponible gracias al middleware aplicado en las rutas
    if (!req.files) {
      return res.status(400).send('No se ha proporcionado ningún archivo');
    }

    const formId = req.params.form_id; // Si esperas obtener esto de la ruta, asegúrate de que esté definido en tus rutas
    const bucket = admin.storage().bucket();
    const promises = [];

    req.files.forEach(file => {
      const fileName = `${Date.now()}-${file.originalname}`;
      const fileUpload = bucket.file(fileName);

      const stream = fileUpload.createWriteStream({
        metadata: {
          contentType: file.mimetype,
        },
        public: true,
      });

      const promise = new Promise((resolve, reject) => {
        stream.on('error', (err) => {
          reject(err);
        });

        stream.on('finish', async () => {
          await fileUpload.makePublic();
          const url = `https://storage.googleapis.com/${bucket.name}/${fileUpload.name}`;
          resolve({ url: url, form_id: formId });
        });

        stream.end(file.buffer);
      });

      promises.push(promise);
    });
    Promise.all(promises)
      .then(results => {
        res.status(200).json(results);
      })
      .catch(error => {
        res.status(500).json({ error: error.toString() });
      });
  }
}

export default new UploadController();
