// En tu controlador o donde manejes las rutas
import { Request, Response } from "express";
import { getRepository } from "fireorm";
import Formularios from "../entity/Formulario";
import Sesion from "../entity/sesiones/Sesion";
class FormularioController {
  async crearFormulario(req: Request, res: Response) {
    try {
      const {
        id,
        nombreFormulario,
        descripcionFormulario,
        fechaCreacion,
        fechaActualizacion,
        status
      } = req.body;

      // Validar que se proporcionen los datos necesarios
      if (!nombreFormulario || !fechaCreacion || !fechaActualizacion) {
        return res
          .status(400)
          .json({ error: "Faltan datos necesarios para crear la guía." });
      }

      if (id) {
        // If id is provided, return an error as it should be handled by a separate update service
        return res.status(400).json({ error: "El campo 'id' no debe estar presente al crear una guía." });
      }

      const guiaRepository = getRepository(Formularios);
      const nuevaGuia = new Formularios();
      nuevaGuia.nombreFormulario = nombreFormulario;
      nuevaGuia.descripcionFormulario = descripcionFormulario;
      nuevaGuia.fechaCreacion = fechaCreacion;
      nuevaGuia.fechaActualizacion = fechaActualizacion;
      nuevaGuia.status = status;

      await guiaRepository.create(nuevaGuia);

      return res.status(201).json({ mensaje: "Guía creada exitosamente.", guiaId: nuevaGuia.id });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: "Error interno del servidor." });
    }
  }

  async modificarFormulario(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const { nombreFormulario, descripcionFormulario, fechaCreacion, fechaActualizacion, status, sesiones } = req.body;

      // Validar que se proporcionen los datos necesarios
      const requiredFields = ['nombreFormulario', 'descripcionFormulario', 'fechaCreacion', 'fechaActualizacion', 'status', 'sesiones'];

      for (const field of requiredFields) {
        if (!req.body[field]) {
          return res.status(400).json({ error: `El campo ${field} es obligatorio para modificar el formulario.` });
        }
      }

      // Validar que sesiones sea un array no vacío
      if (!sesiones || !Array.isArray(sesiones) || sesiones.length === 0) {
        return res.status(400).json({ error: 'La propiedad "sesiones" debe ser un array no vacío.' });
      }

      // Validar que cada sesión tenga campos
      if (!sesiones.every((sesion: Sesion) => sesion.campos && sesion.campos.length > 0)) {
        return res.status(400).json({ error: 'Cada sesión debe tener al menos un campo.' });
      }


      // Obtener el repositorio de FireORM para la entidad Guias
      const guiasRepository = getRepository(Formularios);

      // Obtener la guía existente desde el repositorio
      const guiaExistente = await guiasRepository.findById(id);

      if (!guiaExistente) {
        return res.status(404).json({ error: 'Formulario no encontrado.' });
      }

      // Actualizar los campos de la guía
      guiaExistente.nombreFormulario = nombreFormulario;
      guiaExistente.descripcionFormulario = descripcionFormulario;
      guiaExistente.fechaCreacion = fechaCreacion;
      let date = new Date()
      let day = `${(date.getDate())}`.padStart(2, '0');
      let month = `${(date.getMonth() + 1)}`.padStart(2, '0');
      let year = date.getFullYear();
      guiaExistente.fechaActualizacion = `${year}-${month}-${day}`;
      guiaExistente.status = status;
      guiaExistente.sesiones = sesiones;

      // Guardar los cambios en la base de datos
      await guiasRepository.update(guiaExistente);

      return res.status(200).json({ mensaje: 'Formulario modificado exitosamente.' });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: 'Error interno del servidor.' });
    }
  }


  async consultarFormulario(req: Request, res: Response) {
    try {
      const { id } = req.params; // Suponiendo que el id de la guía se pasa como parámetro en la URL

      // Validar que se proporciona un ID válido
      if (!id) {
        return res
          .status(400)
          .json({ error: "Se requiere un ID válido para consultar el formulario." });
      }

      // Consultar la guía desde la base de datos
      const formulario = await getRepository(Formularios).findById(id);

      // Verificar si la guía existe
      if (!formulario) {
        return res.status(404).json({ error: "Formulario no encontrado." });
      }

      // Devolver la guía consultada
      return res.status(200).json({ formulario });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: "Error interno del servidor." });
    }
  }

  async eliminarFormulario(req: Request, res: Response) {
    try {
      const { id } = req.params;

      if (!id) {
        return res
          .status(400)
          .json({ error: "Se requiere un ID válido para eliminar el formulario." });
      }

      // Attempt to delete the guía from the database
      await getRepository(Formularios).delete(id);

      // Check if the guía was deleted successfully
      // Fireorm delete() does not provide information about the affected rows, so assume success if no error is thrown
      return res.status(200).json({ mensaje: "Formulario eliminado exitosamente." });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: "Error interno del servidor." });
    }
  }

  async listarFormularios(req: Request, res: Response) {
    try {
      // Obtener todas las guías desde la base de datos
      const formularios = await getRepository(Formularios).find();

      // Mapear las guías para obtener solo las propiedades específicas
      const formulariosReducidos = formularios.map((formulario) => ({
        id: formulario.id,
        nombreFormulario: formulario.nombreFormulario,
        descripcionFormulario: formulario.descripcionFormulario,
        fechaCreacion: formulario.fechaCreacion,
        fechaActualizacion: formulario.fechaActualizacion,
        status: formulario.status,
      }));

      // Devolver la lista de guías reducida
      return res.status(200).json({ formularios: formulariosReducidos });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: "Error interno del servidor." });
    }
  }


  async listarFormulariosActivos(req: Request, res: Response) {
    try {
      // Obtener todas las guías activas desde la base de datos
      const formulariosActivos = await getRepository(Formularios).whereEqualTo("status", "activo").find();

      // Mapear las guías activas para obtener solo las propiedades específicas
      const formulariosActivosReducidos = formulariosActivos.map((formulario) => ({
        id: formulario.id,
        nombreFormulario: formulario.nombreFormulario,
        descripcionFormulario: formulario.descripcionFormulario,
        fechaCreacion: formulario.fechaCreacion,
        fechaActualizacion: formulario.fechaActualizacion,
        status: formulario.status,
      }));

      // Devolver la lista de guías activas reducida
      return res.status(200).json({ formulariosActivos: formulariosActivosReducidos });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: "Error interno del servidor." });
    }
  }

  async listarFormulariosActivosMovil(req: Request, res: Response) {
    try {
      // Obtener todas las guías activas desde la base de datos
      const formulariosActivos = await getRepository(Formularios).whereEqualTo("status", "activo").find();

      // Devolver la lista de guías activas reducida
      return res.status(200).json(formulariosActivos);
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: "Error interno del servidor." });
    }
  }


}
export default new FormularioController();
