import { Request, Response } from "express";
import { getRepository } from "fireorm";
import RespuestaFormulario from "../entity/RespuestaFormulario";

class RepuestasController {

    async crearRespuestasFormulario(req: Request, res: Response) {
        try {
            const {
                formulario, // Asume que esta es la referencia o ID al formulario
                datosPersona,
                sesiones
            } = req.body;

            // Validación básica
            if (!formulario || !datosPersona || !sesiones) {
                return res.status(400).json({ error: "Faltan datos necesarios para crear las respuestas del formulario." });
            }

            // Obtener el repositorio
            const respuestaFormularioRepo = getRepository(RespuestaFormulario);

            // Crear una nueva instancia de RespuestaFormulario
            const nuevaRespuestaFormulario = new RespuestaFormulario();
            nuevaRespuestaFormulario.formulario = formulario;
            nuevaRespuestaFormulario.datosPersona = datosPersona;
            nuevaRespuestaFormulario.sesiones = sesiones;

            // Guardar la nueva respuesta en Firestore
            await respuestaFormularioRepo.create(nuevaRespuestaFormulario);

            // Respuesta exitosa
            return res.status(201).json({ mensaje: "Respuesta al formulario creada exitosamente." });
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: "Error interno del servidor." });
        }
    }
    async actualizarRespuestaFormulario(req: Request, res: Response) {
        try {
            const { id } = req.params; // Asume que el ID de la respuesta a actualizar viene en los parámetros de la ruta
            const { datosPersona, sesiones } = req.body;

            // Validación básica
            if (!datosPersona || !sesiones) {
                return res.status(400).json({ error: "Faltan datos necesarios para actualizar la respuesta del formulario." });
            }

            const respuestaFormularioRepo = getRepository(RespuestaFormulario);

            // Buscar la respuesta existente por ID
            const respuestaFormularioExistente = await respuestaFormularioRepo.findById(id);

            if (!respuestaFormularioExistente) {
                return res.status(404).json({ error: "Respuesta de formulario no encontrada." });
            }

            // Actualizar la respuesta existente con los nuevos valores
            respuestaFormularioExistente.datosPersona = datosPersona;
            respuestaFormularioExistente.sesiones = sesiones;

            // Guardar la respuesta actualizada en Firestore
            await respuestaFormularioRepo.update(respuestaFormularioExistente);

            // Respuesta exitosa
            return res.json({ mensaje: "Respuesta al formulario actualizada exitosamente." });
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: "Error interno del servidor." });
        }
    }
    async obtenerTodasLasRespuestas(req: Request, res: Response) {
        // Función para obtener el índice de una sesión
        function obtenerIndice(nombreSesion: string): number | null {
            const match = nombreSesion.match(/^\d+\.\d+/);
            if (match) {
                return parseFloat(match[0]);
            }
            return null;
        }

        // Función para obtener el grupo de un índice
        function obtenerGrupo(indice: number | null): string | null {
            if (indice !== null && indice !== 1) { // Excluir el grupo 0
                return Math.floor(indice).toString();
            }
            return null;
        }

        // Función para combinar las métricas de sectores iguales
        function combinarMetricasPorSector(metricas) {
            const resultado = {};

            // Objeto para almacenar los sectores combinados
            const sectoresCombinados = {};

            // Iterar sobre cada sector
            for (const sectorKey in metricas) {
                const sector = sectorKey.split(/[0-9]/)[0]; // Eliminar números del nombre del sector
                const datos = metricas[sectorKey];
                if (!sectoresCombinados[sector]) {
                    sectoresCombinados[sector] = [];
                }
                // Agregar los datos del sector al objeto de sectores combinados
                sectoresCombinados[sector].push(datos);
            }

            // Iterar sobre los sectores combinados y calcular promedios
            for (const sector in sectoresCombinados) {
                const datosSector = sectoresCombinados[sector];
                const promedioGrupo = [];
                // Iterar sobre los grupos y calcular el promedio
                for (let i = 0; i < datosSector[0].length; i++) {
                    let suma = 0;
                    let contador = 0;
                    // Sumar los valores para el mismo grupo de diferentes sectores
                    for (let j = 0; j < datosSector.length; j++) {
                        suma += parseFloat(datosSector[j][i].promedio);
                        contador++;
                    }
                    promedioGrupo.push({
                        grupo: datosSector[0][i].grupo,
                        promedio: (suma / contador).toFixed(2) // Calcular el promedio
                    });
                }
                resultado[sector] = promedioGrupo; // Agregar el promedio al resultado final
            }

            return resultado;
        }

        try {
            const respuestaFormularioRepo = getRepository(RespuestaFormulario);

            // Buscar todas las respuestas almacenadas
            const respuestas = await respuestaFormularioRepo.find();
            const respuestasAgrupadas = {}; // Objeto para almacenar las respuestas agrupadas por sector

            // Agrupar respuestas por sector
            respuestas.forEach(respuesta => {
                if (respuesta.sesiones.find(sesion => sesion.nombre === '0.1 Datos Generales')) {
                    const sector = respuesta.sesiones.find(sesion => sesion.nombre === '0.1 Datos Generales')
                        .respuesta.find(resp => resp.nombre === 'sector').respuesta.valor;

                    if (respuestasAgrupadas[sector]) {
                        // Si el sector ya existe en respuestasAgrupadas
                        let count = 1;
                        let newSector;
                        do {
                            newSector = `${sector}${count}`;
                            count++;
                        } while (respuestasAgrupadas[newSector]);

                        respuestasAgrupadas[newSector] = [];
                        respuestasAgrupadas[newSector].push(respuesta);
                    } else {
                        // Si el sector no existe en respuestasAgrupadas
                        respuestasAgrupadas[sector] = [];
                        respuestasAgrupadas[sector].push(respuesta);
                    }
                }
            });

            const metricasPorSector = {};
            Object.keys(respuestasAgrupadas).forEach(sector => {
                const respuestasSector = respuestasAgrupadas[sector];
                const gruposIndices = {};

                respuestasSector.forEach(respuesta => {
                    respuesta.sesiones.forEach(sesion => {
                        const indice = obtenerIndice(sesion.nombre);
                        const grupo = obtenerGrupo(indice);

                        if (grupo && !(grupo in gruposIndices)) {
                            gruposIndices[grupo] = { totalValor: 0, contadorRespuestas: 0 };
                            let i = 0;
                            sesion.respuesta.forEach(resp => {
                                if (Array.isArray(resp.respuesta) && i === 0) {
                                    resp.respuesta.forEach(subResp => {
                                        if (subResp && subResp.valor !== undefined) {
                                            gruposIndices[grupo].totalValor += subResp.valor;
                                            gruposIndices[grupo].contadorRespuestas++;
                                        }
                                    });
                                    i++;
                                } else if (resp.respuesta && resp.respuesta.valor !== undefined) {
                                    gruposIndices[grupo].totalValor += resp.respuesta.valor;
                                    gruposIndices[grupo].contadorRespuestas++;
                                }
                            });
                        }
                    });
                });
                // Calcular el promedio y dar formato para cada grupo de índices
                metricasPorSector[sector] = Object.keys(gruposIndices)
                    .filter(grupo => grupo !== '0')
                    .map(grupo => {
                        const totalValor = gruposIndices[grupo].totalValor;
                        return {
                            grupo,
                            promedio: (totalValor * 100).toFixed(2) // Redondear al entero más cercano
                        };
                    });
            });


            // Llamar a la función para combinar las métricas
            const metricasCombinadas = combinarMetricasPorSector(metricasPorSector);
            return res.json(metricasCombinadas);

        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: "Error interno del servidor." });
        }
    }
    async obtenerTodasLasRespuestas2(req, res) {

        try {
            const { nombreRegion } = req.params; // Obtener el nombre de la finca de los parámetros de la URL

            const respuestaFormularioRepo = getRepository(RespuestaFormulario);

            // Buscar todas las respuestas almacenadas
            const respuestas = await respuestaFormularioRepo.find();

            // Filtrar las respuestas solo para la finca específica
            const respuestasRegion = respuestas.filter(respuesta => {
                if (respuesta.sesiones.find(sesion => sesion.nombre === '0.1 Datos Generales')) {
                    const nombreFincaRespuesta = respuesta.sesiones.find(sesion => sesion.nombre === '0.1 Datos Generales')
                        .respuesta.find(resp => resp.nombre === 'sector').respuesta.valor;
                    if (nombreFincaRespuesta === nombreRegion) {
                        console.log(nombreFincaRespuesta)
                        return respuesta;
                    }
                }
            });

            // Objeto para mantener los totales y las cantidades de cada sesión
            const sesionesTotales = {};
            // Agrupar las respuestas según el primer número en el nombre de la sesión
            respuestasRegion.forEach(respuesta => {

                respuesta.sesiones.forEach(sesion => {
                    const nombreSesion = sesion.nombre;
                    const primerNumero = parseInt(nombreSesion.split('.')[0]); // Obtener el primer número en el nombre de la sesión
                    if (!sesionesTotales[primerNumero]) {
                        sesionesTotales[primerNumero] = [];
                    }
                    let totalValor = 0;
                    let contadorRespuestas = 0;
                    let i = 0;
                    sesion.respuesta.forEach(resp => {
                        if (Array.isArray(resp.respuesta) && i === 0) { // Verificar si la respuesta es un array de opciones múltiples
                            resp.respuesta.forEach(subResp => {
                                if (subResp && subResp.valor !== undefined) { // Verificar si el objeto subResp no es nulo

                                    totalValor += subResp.valor;
                                    contadorRespuestas++;
                                }
                            });
                            i++;
                        } else if (resp.respuesta && resp.respuesta.valor !== undefined) { // Verificar si el objeto resp.respuesta no es nulo
                            totalValor += resp.respuesta.valor;
                            contadorRespuestas++;
                        }
                    });
                    //console.log(totalValor)
                    // Verificar si la sesión ya ha sido agregada al arreglo correspondiente
                    const sesionExistenteIndex = sesionesTotales[primerNumero].findIndex(s => s.nombreSesion === nombreSesion);
                    if (sesionExistenteIndex !== -1) {
                        let valorAjustado = totalValor * 100;
                        let valortotal = sesionesTotales[primerNumero][sesionExistenteIndex].promedio;

                        let suma = valorAjustado + valortotal;
                        // Si la sesión ya existe, agregar los valores y actualizar el contador de respuestas
                        sesionesTotales[primerNumero][sesionExistenteIndex].promedio = (suma / 2).toFixed(2);

                    } else {
                        let valorAjustado = totalValor * 100;
                        sesionesTotales[primerNumero].push({
                            nombreSesion: nombreSesion,
                            promedio: valorAjustado,
                        });
                    }


                });
            });

            // Convertir el objeto sesionesTotales en un arreglo de arreglos de sesiones
            const metricas = Object.values(sesionesTotales);

            // Devolver las respuestas agrupadas
            return res.json(metricas);
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: "Error interno del servidor." });
        }
    }
    async obtenerTodasLasRespuestas3(req: Request, res: Response) {
        // Función para obtener el índice de una sesión
        function obtenerIndice(nombreSesion: string): number | null {
            const match = nombreSesion.match(/^\d+\.\d+/);
            if (match) {
                return parseFloat(match[0]);
            }
            return null;
        }

        // Función para obtener el grupo de un índice
        function obtenerGrupo(indice: number | null): string | null {
            if (indice !== null && indice !== 1) { // Excluir el grupo 0
                return Math.floor(indice).toString();
            }
            return null;
        }

        function combinarMetricasPorSector(metricas) {
            const resultado = {};

            // Objeto para almacenar los sectores combinados
            const sectoresCombinados = {};

            // Iterar sobre cada sector
            for (const sectorKey in metricas) {
                const sector = sectorKey.split(/[0-9]/)[0]; // Eliminar números del nombre del sector
                const datos = metricas[sectorKey];
                if (!sectoresCombinados[sector]) {
                    sectoresCombinados[sector] = [];
                }
                // Agregar los datos del sector al objeto de sectores combinados
                sectoresCombinados[sector].push(datos);
            }

            // Iterar sobre los sectores combinados y calcular promedios
            for (const sector in sectoresCombinados) {
                const datosSector = sectoresCombinados[sector];
                const promedioGrupo = [];
                // Iterar sobre los grupos y calcular el promedio
                for (let i = 0; i < datosSector[0].length; i++) {
                    let suma = 0;
                    let contador = 0;
                    // Sumar los valores para el mismo grupo de diferentes sectores
                    for (let j = 0; j < datosSector.length; j++) {
                        suma += parseFloat(datosSector[j][i].promedio);
                        contador++;
                    }
                    promedioGrupo.push({
                        grupo: datosSector[0][i].grupo,
                        promedio: (suma / contador).toFixed(2) // Calcular el promedio
                    });
                }
                resultado[sector] = promedioGrupo; // Agregar el promedio al resultado final
            }

            return resultado;
        }
        try {
            const respuestaFormularioRepo = getRepository(RespuestaFormulario);

            // Buscar todas las respuestas almacenadas
            const respuestas = await respuestaFormularioRepo.find();
            const respuestasAgrupadas = {}; // Objeto para almacenar las respuestas agrupadas por nombre de finca

            // Agrupar respuestas por nombre de finca
            respuestas.forEach(respuesta => {
                if (respuesta.sesiones.find(sesion => sesion.nombre === '0.1 Datos Generales')) {
                    const nombreFinca = respuesta.sesiones.find(sesion => sesion.nombre === '0.1 Datos Generales')
                        .respuesta.find(resp => resp.nombre === 'nombrefinca').respuesta.valor;
                    if (respuestasAgrupadas[nombreFinca]) {
                        // Si el sector ya existe en respuestasAgrupadas
                        let count = 1;
                        let newSector;
                        do {
                            newSector = `${nombreFinca}${count}`;
                            count++;
                        } while (respuestasAgrupadas[newSector]);

                        respuestasAgrupadas[newSector] = [];
                        respuestasAgrupadas[newSector].push(respuesta);
                    } else {
                        // Si el sector no existe en respuestasAgrupadas
                        respuestasAgrupadas[nombreFinca] = [];
                        respuestasAgrupadas[nombreFinca].push(respuesta);
                    }
                }
            });

            // Calcular métricas para cada finca
            const metricasPorFinca = {};
            Object.keys(respuestasAgrupadas).forEach(nombrefinca => {
                const respuestasFinca = respuestasAgrupadas[nombrefinca];
                const gruposIndices = {};

                respuestasFinca.forEach(respuesta => {
                    respuesta.sesiones.forEach(sesion => {
                        const indice = obtenerIndice(sesion.nombre);
                        const grupo = obtenerGrupo(indice);

                        if (grupo && !(grupo in gruposIndices)) {
                            gruposIndices[grupo] = { totalValor: 0, contadorRespuestas: 0 };
                            let i = 0;
                            sesion.respuesta.forEach(resp => {
                                if (Array.isArray(resp.respuesta) && i === 0) {
                                    resp.respuesta.forEach(subResp => {
                                        if (subResp && subResp.valor !== undefined) {
                                            gruposIndices[grupo].totalValor += subResp.valor;
                                            gruposIndices[grupo].contadorRespuestas++;
                                        }
                                    });
                                    i++;
                                } else if (resp.respuesta && resp.respuesta.valor !== undefined) {
                                    gruposIndices[grupo].totalValor += resp.respuesta.valor;
                                    gruposIndices[grupo].contadorRespuestas++;
                                }
                            });
                        }
                    });
                });
                // Calcular el promedio y dar formato para cada grupo de índices
                metricasPorFinca[nombrefinca] = Object.keys(gruposIndices)
                    .filter(grupo => grupo !== '0')
                    .map(grupo => {
                        const totalValor = gruposIndices[grupo].totalValor;
                        return {
                            grupo,
                            promedio: (totalValor * 100).toFixed(2) // Redondear al entero más cercano
                        };
                    });
            });
            // Llamar a la función para combinar las métricas
            const metricasCombinadas = combinarMetricasPorSector(metricasPorFinca);
            return res.json(metricasCombinadas);
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: "Error interno del servidor." });
        }
    }
    async obtenerMetricasPorFinca(req: Request, res: Response) {

        try {
            const { nombreFinca } = req.params; // Obtener el nombre de la finca de los parámetros de la URL

            const respuestaFormularioRepo = getRepository(RespuestaFormulario);

            // Buscar todas las respuestas almacenadas
            const respuestas = await respuestaFormularioRepo.find();

            // Filtrar las respuestas solo para la finca específica
            const respuestasFinca = respuestas.filter(respuesta => {
                if (respuesta.sesiones.find(sesion => sesion.nombre === '0.1 Datos Generales')) {
                    const nombreFincaRespuesta = respuesta.sesiones.find(sesion => sesion.nombre === '0.1 Datos Generales')
                        .respuesta.find(resp => resp.nombre === 'nombrefinca').respuesta.valor
                    if (nombreFincaRespuesta === nombreFinca) {
                        return respuesta;
                    }
                }
            });

            // Objeto para mantener los totales y las cantidades de cada sesión
            const sesionesTotales = {};
            // Agrupar las respuestas según el primer número en el nombre de la sesión
            respuestasFinca.forEach(respuesta => {

                respuesta.sesiones.forEach(sesion => {
                    const nombreSesion = sesion.nombre;
                    const primerNumero = parseInt(nombreSesion.split('.')[0]); // Obtener el primer número en el nombre de la sesión
                    if (!sesionesTotales[primerNumero]) {
                        sesionesTotales[primerNumero] = [];
                    }
                    let totalValor = 0;
                    let contadorRespuestas = 0;
                    let i = 0;
                    sesion.respuesta.forEach(resp => {
                        if (Array.isArray(resp.respuesta) && i === 0) { // Verificar si la respuesta es un array de opciones múltiples
                            resp.respuesta.forEach(subResp => {
                                if (subResp && subResp.valor !== undefined) { // Verificar si el objeto subResp no es nulo

                                    totalValor += subResp.valor;
                                    contadorRespuestas++;
                                }
                            });
                            i++;
                        } else if (resp.respuesta && resp.respuesta.valor !== undefined) { // Verificar si el objeto resp.respuesta no es nulo
                            totalValor += resp.respuesta.valor;
                            contadorRespuestas++;
                        }
                    });
                    //console.log(totalValor)
                    // Verificar si la sesión ya ha sido agregada al arreglo correspondiente
                    const sesionExistenteIndex = sesionesTotales[primerNumero].findIndex(s => s.nombreSesion === nombreSesion);
                    if (sesionExistenteIndex !== -1) {
                        let valorAjustado = totalValor * 100;
                        let valortotal = sesionesTotales[primerNumero][sesionExistenteIndex].promedio;

                        let suma = valorAjustado + valortotal;
                        // Si la sesión ya existe, agregar los valores y actualizar el contador de respuestas
                        sesionesTotales[primerNumero][sesionExistenteIndex].promedio = (suma / 2).toFixed(2);

                    } else {
                        let valorAjustado = totalValor * 100;
                        sesionesTotales[primerNumero].push({
                            nombreSesion: nombreSesion,
                            promedio: valorAjustado,
                        });
                    }


                });
            });

            // Convertir el objeto sesionesTotales en un arreglo de arreglos de sesiones
            const metricas = Object.values(sesionesTotales);

            // Devolver las respuestas agrupadas
            return res.json(metricas);
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: "Error interno del servidor." });
        }
    }
    async obtenerMetricasPorFincaDetail(req, res) {
        try {
            const { nombreFinca } = req.params;
    
            const respuestaFormularioRepo = getRepository(RespuestaFormulario);
    
            const respuestas = await respuestaFormularioRepo.find();
    
            let formularioEncontrado = null;
    
            respuestas.forEach(respuesta => {
                if (!formularioEncontrado && respuesta.sesiones.find(sesion => sesion.nombre === '0.1 Datos Generales')) {
                    const nombreFincaRespuesta = respuesta.sesiones.find(sesion => sesion.nombre === '0.1 Datos Generales')
                        .respuesta.find(resp => resp.nombre === 'nombrefinca').respuesta.valor;
                    if (nombreFincaRespuesta === nombreFinca) {
                        formularioEncontrado = respuesta;
                    }
                }
            });
    
            if (!formularioEncontrado) {
                // Si no se encuentra ningún formulario con el nombre de finca proporcionado
                return res.status(404).json({ error: "No se encontró ningún formulario para la finca proporcionada." });
            }
    
            // Devolver el formulario encontrado
            return res.json({ formularioEncontrado });
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: "Error interno del servidor." });
        }
    }
    async obtenerMetricasPorSectorDetail(req, res) {
        try {
            const { nombreSector } = req.params;
    
            const respuestaFormularioRepo = getRepository(RespuestaFormulario);
    
            const respuestas = await respuestaFormularioRepo.find();
    
            const formulariosEncontrados = [];
    
            respuestas.forEach(respuesta => {
                if (respuesta.sesiones.find(sesion => sesion.nombre === '0.1 Datos Generales')) {
                    const sector = respuesta.sesiones.find(sesion => sesion.nombre === '0.1 Datos Generales')
                        .respuesta.find(resp => resp.nombre === 'sector').respuesta.valor;
                    if (sector === nombreSector) {
                        formulariosEncontrados.push(respuesta);
                    }
                }
            });
    
            if (formulariosEncontrados.length === 0) {
                // Si no se encuentra ningún formulario con el nombre de sector proporcionado
                return res.status(404).json({ error: "No se encontró ningún formulario para el sector proporcionado." });
            }
    
            // Eliminar elementos numéricos que no tienen un valor definido en cada formulario
            formulariosEncontrados.forEach(formulario => {
                formulario.sesiones.forEach(sesion => {
                    sesion.respuesta = sesion.respuesta.filter(resp => typeof resp !== 'number' || (typeof resp === 'number' && sesion.respuesta[sesion.respuesta.indexOf(resp) + 1] && sesion.respuesta[sesion.respuesta.indexOf(resp) + 1].valor !== undefined));
                });
            });
    
            // Devolver los formularios encontrados
            return res.json({ formulariosEncontrados });
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: "Error interno del servidor." });
        }
    }
    async obtenerMetricasPorUsuario(req, res) {
        try {
            const { id } = req.params;
    
            const respuestaFormularioRepo = getRepository(RespuestaFormulario);
    
            const respuestas = await respuestaFormularioRepo.find();
    
            let formularioEncontrado = [];
    
            respuestas.forEach(respuesta => {
                if(respuesta.datosPersona.find(usuario => usuario.id_usuario === id)){
                    const formularioUsuario = respuesta.datosPersona.find(usuario => usuario.id_usuario === id);
                    if (formularioUsuario  ) {
                        // Si se encuentra el formulario del usuario, se asigna a formularioEncontrado
                        formularioEncontrado.push(respuesta);
                    }
                }
            });
    
            if (formularioEncontrado.length === 0) {
                // Si no se encuentra ningún formulario con el nombre de finca proporcionado
                return res.status(404).json({ error: "No se encontró ningún formulario del usuario proporcionada." });
            }
    
            // Devolver el formulario encontrado
            return res.json({ formularioEncontrado });
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: "Error interno del servidor." });
        }
    }
    
}
export default new RepuestasController();