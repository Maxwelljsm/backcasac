import { Request, Response } from "express";
import { getRepository } from "fireorm";
import Formulario from "../entity/Formulario";

class DespliegueComparacionController {
  async activarFormularios(req: Request, res: Response) {
    try {
      const { ids } = req.body;

      // Validar que se proporcionen los datos necesarios
      if (!Array.isArray(ids) || ids.length === 0) {
        return res.status(400).json({ error: 'Se requiere una lista válida de IDs de formularios.' });
      }

      // Obtener el repositorio de FireORM para la entidad Formulario
      const formulariosRepository = getRepository(Formulario);

      // Obtener todos los formularios
      const todosLosFormularios = await formulariosRepository.find();

      // Actualizar el estado de los formularios según la lista de IDs proporcionados
      const formulariosActualizados = todosLosFormularios.map(async (formulario) => {
        if (ids.includes(formulario.id)) {
          // Si el formulario está en la lista, establecer el estado a "activo"
          formulario.status = 'activo';
        } else {
          // Si el formulario no está en la lista, establecer el estado a "inactivo"
          formulario.status = 'inactivo';
        }

        // Guardar los cambios en la base de datos
        await formulariosRepository.update(formulario);
        return formulario;
      });

      await Promise.all(formulariosActualizados);

      return res.status(200).json({ mensaje: 'Formularios actualizados exitosamente.' });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: 'Error interno del servidor.' });
    }
  }
}

export default new DespliegueComparacionController();
