import { Router } from "express";
import RespuestasController from "../controller/RespuestasController";


const router = Router();

router.post('/create_response', RespuestasController.crearRespuestasFormulario);
router.put('/update_response', RespuestasController.actualizarRespuestaFormulario)
router.get('/reporter_metric', RespuestasController.obtenerTodasLasRespuestas)
router.get('/reporter_metric2/:nombreRegion', RespuestasController.obtenerTodasLasRespuestas2)
router.get('/reporter_metric3', RespuestasController.obtenerTodasLasRespuestas3)
router.get('/reporter_name/:nombreFinca', RespuestasController.obtenerMetricasPorFinca)
router.get('/reporter_name/:nombreFinca/detail', RespuestasController.obtenerMetricasPorFincaDetail)
router.get('/reporter_nameSec/:nombreSector/detail', RespuestasController.obtenerMetricasPorSectorDetail)
router.get('/reporter_usuario/:id', RespuestasController.obtenerMetricasPorUsuario)
export default router;