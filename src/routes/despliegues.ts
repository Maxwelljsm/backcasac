import { Router } from "express";
import DespliCompController from "../controller/DesplieguesController";

const router = Router();

router.put('/desplegar', DespliCompController.activarFormularios);

export default router;