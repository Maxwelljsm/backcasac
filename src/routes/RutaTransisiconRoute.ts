import { Router } from "express";
import RutaTransicionController from "../controller/RutaTransicionController";

const router = Router();
router.post('/Transicionreport', RutaTransicionController.CrearRutaTransicion);
router.put('/modificar/:nombre', RutaTransicionController.modificarRutaTransicion);
router.get('/consultar/:nombre', RutaTransicionController.consultarRutaTransicion);
export default router;