import { Router } from "express";
import FormulariosController from "../controller/FormulariosController";

const router = Router();

router.post('/create', FormulariosController.crearFormulario);
router.put('/modificar/:id', FormulariosController.modificarFormulario);
router.get('/consultar/:id', FormulariosController.consultarFormulario);
router.delete('/eliminar/:id', FormulariosController.eliminarFormulario);
router.get('/listar',FormulariosController.listarFormularios);
router.get('/listarActivos',FormulariosController.listarFormulariosActivos);
router.get('/listarActivosMov',FormulariosController.listarFormulariosActivosMovil);
export default router;