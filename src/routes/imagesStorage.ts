import { Router } from "express";
import imagesStorage from "../controller/ImagesStorageController";


const router = Router();
router.post('/upload_image/:form_id',imagesStorage.uploadMiddleware, imagesStorage.subirImagen);



export default router;