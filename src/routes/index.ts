import { Router } from "express";
import auth from "./auth";
import formulario from "./formularios";
import users from "./users";
import despliegues from "./despliegues"
import respuestas from "./respuestas"
import storage from "./imagesStorage"
import rutatransicion from "./RutaTransisiconRoute";
const routes = Router();

routes.use('/auth', auth);
routes.use('/form', formulario);
routes.use('/users', users)
routes.use('/sync', despliegues)
routes.use('/response', respuestas)
routes.use('/storage', storage)
routes.use('/ruta',rutatransicion);
export default routes;