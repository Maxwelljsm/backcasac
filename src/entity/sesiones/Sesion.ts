import Campo from "./Campo";
class Sesion {
  nombre: string;
  descripcion: string;
  campos: Campo[];
}
export default Sesion;