class Opcion {
  etiqueta: string;
  valor: string | number | boolean; // Adjust the value type based on your requirements
}
export default Opcion;