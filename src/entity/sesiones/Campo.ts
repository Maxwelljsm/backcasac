import Fila from "./Fila";
import Opcion from "./Opcion";
import Columna from "./Columna";
import Dependencia from "./Dependecias";

class Campo {
    etiqueta: string;
    nombre: string;
    placeholder: string;
    requerido: boolean;
    tipo: string;
    dependencia?: Dependencia;
    opciones?: Opcion[]; // Add an optional property for fields with options
    filas?: Fila[]; // Add an optional property for matrix rows
    columnas?: Columna[]; // Add an optional property for matrix columns
}
export default Campo;