import { Collection, getRepository } from 'fireorm';
import SesionRuta from './sesiones/SesionRuta';
import { Tracing } from 'trace_events';

@Collection()
class RespuestaTransicions {
    id: string;
    nombre:string;
    fechaCreacion:string;
    sesiones: SesionRuta[]; // Considera definir una clase o interface más específica
}

export default RespuestaTransicions;