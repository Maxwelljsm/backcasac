import { Collection, getRepository } from 'fireorm';

@Collection()
class RespuestaFormulario {
    id: string;
    formulario:any;
    datosPersona: any; // Considera definir una clase o interface más específica
    sesiones: any[]; // Considera definir una clase o interface más específica
}

export default RespuestaFormulario;