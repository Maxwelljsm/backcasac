import { Collection } from "fireorm";
import Sesion from "./sesiones/Sesion";
@Collection()
class Formularios {
    id: string;
    nombreFormulario: string; // Change from titulo_guia to nombreFormulario
    descripcionFormulario:string; 
    fechaCreacion: string; // Change from fecha_creacion to fechaCreacion
    fechaActualizacion: string; // Change from fecha_modificacion to fechaActualizacion
    status: string;
    sesiones: Sesion[];
}

export default Formularios;
